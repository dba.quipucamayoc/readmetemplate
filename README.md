# Aplicación de marcación de asistencia (App Frontend)

### Dependencias

- Node 8
- Desarrollado con el framework: __Angular 1.6.2__

### Variables de Entorno

| Variable                | Descripcion                                     | Ejemplo                      |
|-------------------------|-------------------------------------------------|------------------------------|
| DOMAIN_BACKEND_API_WEB  | Ruta base del Sistema Backend API-Web           | 'http://localhost:3000'      |
| URL_API                 | Ruta base del Sistema Frontend API-Web          | 'http://localhost:8080'      |
| production              | Indica si el sistema se encuentra en producción | false                        |


### Despliegue Frontend (Produccion)
- Estar en la raíz del proyecto
```
└── DESARROLLO
    └── CODIGOFUENTE
        └── FRONTEND
```
- Ejecutar el siguiente comando para instalar las dependencias
```
npm install -g bower && bower install && npm install
```


- Ejecutar el siguiente comando para compilar a producción
```
npm run build:pro
```

- Los archivos de la compilación estarán en la siguiente ubicación
```
└── DESARROLLO
    └── CODIGOFUENTE
        └── BACKEND
            └── client
                └── AsistenciaBolsista
```

## Opcional: Documentacion adicional para el desarrollador
### Levantar el proyecto con Docker. **Por defecto está conectado a la api de desarrollo.*
**Si se quiere que esté conectada a la api de producción, modificar en `src/app` el valor de la llave `apiServer`*
```js
App.constant("config", {
    "apiServer": 'https://quipucamayoc.unmsm.edu.pe/AsistenciaBolsista/api/v1/',
    ...
});
```

### Levantar el proyecto solo con el Dockerfile.

- Paso 1: Abrir la terminal en el directorio actual (FRONTEND) y ejecutar el siguiente comando para crear la imagen.
```
docker build --tag <nombre_de_la_imagen> .
```

- Paso 2: Ejecutar el siguiente comando para crear el contenedor de la aplicación.
```
docker container run -it --name <nombre_del_contenedor> --rm -p 43040:43040 <nombre_de_la_imagen>
```

- Paso 3: Ir al navegador a la siguiente dirección:
```
http://localhost:43040
```

- Listo.

### Levantar el proyecto con el docker-compose
- Paso 1: Ejecutar el siguiente comando en el directorio actual (FRONTEND):
```
docker-compose up -d
```
- Paso 2: Ir al navegador a la siguiente dirección:
```
http://localhost:43040
```

- Listo.

### Levantar el proyecto para seguir desarrollando (en vscode)
- Paso 1: Abrir la carpeta, FRONTEND, con el vscode
- Paso 2: Dar clic en el ícono de la parte inferior izquierda (><)
- Paso 3: En las opciones que aparecen dar clic en "Reopen in Container". (Desarrollar)
- Paso 4: Para salir, dar clic en el ícono del paso 2, y elegir la última opción "Close Remote Connection"
- Listo.
* *Para desarrollar con otro editor, ejecutar el siguiente comando y desarrolle en su editor de preferencia.
```
docker-compose -f ./docker-compose-dev.yml up -d

